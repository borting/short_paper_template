# LaTeX thesis template to short paper
Coverting [NCTU LaTeX thesis template](https://gitlab.com/borting/latex_thesis_template) to short paper (IEEE 2-column format for CS conferences)

## Steps
* Download and open main.tex
    * Input thesis title and your name in \title{} and \author{}
    * Copy all input contents commnads, i.e. \input{chapters/...}, from your thesis' main.tex to here, and place them under Contents
    * Replace 'chapters' in \input{...} to 'sections'
        * e.g. \input{chapters/1-intro} --> \input{sections/1-intro}
    * Copy the bib list from your thesis' main.tex to here (i.e. \bibliography{})

* Make a copy for the folder storing the latex source files of your thesis, and in the new folder ...
    * Replace main.tex with the new main.tex updated in the previous step
    * Rename 'chapters' folder to 'sections'

* Download config/fonts.tex
    * Use the new fonts.tex to replace the old one stored under config/

* Open main.tex with texmaker
    * Enter each content file (i.e. \input{sections/...}) and replace sectioning commands with the following order (please use 'Replace All')
        * \subsection --> \subsubsection
        * \section --> \subsection
        * \chapter --> \section

* Done
    * Rebuild main.tex
    * Start to fixing some tiny problems (e.g. out-of-range tables)

## Q&A
* How to solve out-of-range tables
    * Spread these tables to 1-column width
        * \begin{table} --> \begin{table\*}
        * \end{table} --> \end{table\*}

* How to solve longtable is not in 1-column mode?
    * Replace longtable with supertabular
        * \begin{longtable} --> \begin{supertabular}
        * \end{longtable} --> \end{supertabular}
